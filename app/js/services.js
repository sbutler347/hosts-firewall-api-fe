angular.module('FirewallApp.services', [])
  .factory('firewallAPIservice', function($rootScope, $http) {

    var firewallAPI = {};

    firewallAPI.getHosts = function() {

        return $http.get('http://range_dl_firewall_api.scottb.dev.ed.dotlabel.net/api/hosts', {
        headers: {'Authorization': "bearer "+$rootScope.oauth.access_token}
        });
    }


    firewallAPI.enableHost = function(id) {

        return $http.get('http://range_dl_firewall_api.scottb.dev.ed.dotlabel.net/api/hosts/'+id+'/enable', {
        //headers: {'Authorization': authToken}
        });
    }

    firewallAPI.disableHost = function(id) {

        return $http.get('http://range_dl_firewall_api.scottb.dev.ed.dotlabel.net/api/hosts/'+id+'/disable', {
      //  headers: {'Authorization': authToken}
        });

    }

    firewallAPI.isactiveHost = function(host) {

        return $http.get('http://range_dl_firewall_api.scottb.dev.ed.dotlabel.net/api/hosts/'+host+'/active', {
       // headers: {'Authorization': authToken}
        });
    }

    firewallAPI.createHost = function(host) {

        return $http.post('http://range_dl_firewall_api.scottb.dev.ed.dotlabel.net/api/hosts/create', {
        data: { 'hostRange':{ 'name' : host.name, 'subnet' : host.subnet, 'cidr' : host.cidr, 'description' : host.description, 'enabled' : host.enabled}}

        });
    }

    firewallAPI.login = function($scope, host) {

      return $http({
             url: 'http://range_dl_firewall_api.scottb.dev.ed.dotlabel.net/app_dev.php/oauth/v2/token',
             method: "post",
             headers: {'Content-Type': 'application/x-www-form-urlencoded', },
             data: $.param({
             'client_id' : "2_2ud7d4z0e0yswo0ws0c8cks4gss0k48400o0ggg0oosc8gg40g",
             'client_secret' : "1lexfq7776m8wgsso88gk0o08o0g40wwgws0g0wc8k0gg8ow8o",
             'grant_type': 'password',
             'username': $scope.user.username,
             'password': $scope.user.password

           }),
        });
    }


    return firewallAPI;
  });
