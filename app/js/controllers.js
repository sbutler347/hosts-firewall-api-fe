angular.module('FirewallApp.controllers', []).



  /* get all hosts controller */
  controller('hostsController', function($scope, firewallAPIservice) {

    $scope.nameFilter = null;
    $scope.hostsList = [];
    $scope.searchFilter = function (host) {
        var re = new RegExp($scope.nameFilter, 'i');

        return !$hostsList.Host.id || re.test(hostsList.name) || re.test(hostsList.subnet);
    };

      firewallAPIservice.getHosts().success(function (response) {

          $scope.hostsList = response;
          });

  }).

  /* enable Host controller */
  controller('enableHostController', function($scope, firewallAPIservice) {

    $scope.master = {};
    $scope.apiResponse = null;

    $scope.enable = function(host) {
        firewallAPIservice.enableHost($scope.host.id).success(function (response) {
           $scope.apiResponse = response;
        });
     };

    $scope.disable = function(host){
      firewallAPIservice.disableHost($scope.host.id).success(function (response) {
        $scope.apiResponse = response;
      });
    }

  }).

  /* create Host controller */
  controller('createHostController', function($scope, firewallAPIservice) {

    $scope.apiResponse = null;
    $scope.newHost = function(host) {
      firewallAPIservice.createHost($scope.host).success(function (response) {
        $scope.apiResponse = response;
        });
    }
  }).

  /* is active Host controller */
  controller('isactiveHostController', function($scope, firewallAPIservice) {

    $scope.apiResponse = null;
      $scope.isactive = function(host){
      firewallAPIservice.isactiveHost($scope.host.subnet).success(function (response) {
        $scope.apiResponse = response;
      });
      }
  }).

  controller('UserCtrl', function( $scope, $http, firewallAPIservice) {
      $scope.user = {username: 'admin', password: 'admin'};
      $scope.apiResponse = null;
      sessionService = {};

      $scope.login = function(host){
          firewallAPIservice.login($scope).success(function (response) {
              $scope.setToken(response.access_token);
              console.log($scope.getToken());
          });
      },
      $scope.setToken = function(accessToken){
         localStorage['accessToken'] = accessToken;
        },
      $scope.getToken = function(){
         return localStorage['accessToken'];
      };

  })





