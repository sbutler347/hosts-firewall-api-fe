angular.module('FirewallApp', [
  'FirewallApp.services',
  'FirewallApp.controllers',
  'ngRoute'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.
    when("/", {templateUrl: "partials/login.html"}).
	when("/hosts/create", {templateUrl: "partials/create.html"}).
	when("/hosts", {templateUrl: "partials/hosts.html", controller: "hostsController"}).
	when("/hosts/enable", {templateUrl: "partials/enable.html"}).
    when("/hosts/active", {templateUrl: "partials/active.html"}).
	when("/hosts/disable", {templateUrl: "partials/disable.html"});
	//otherwise({redirectTo: '/'});
}]);